package service;

import domain.Gem;

import java.util.Comparator;

public class CompareByValue implements Comparator<Gem> {
    public int compare(Gem o1, Gem o2) {
        return (o1.getValue()>=o2.getValue()?1:-1);
    }
}
