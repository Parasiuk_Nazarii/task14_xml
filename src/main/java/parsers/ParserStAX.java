package parsers;

import domain.Gem;
import domain.Preciousness;
import domain.VisualParameters;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class ParserStAX {
    public void start(){
        boolean bName = false;
        boolean bPreciousness = false;
        boolean bOrigin = false;
        boolean bColor = false;
        boolean bTransparency =false;
        boolean bFaces = false;
        boolean bValue = false;

        Gem gem = new Gem();
        VisualParameters vp = new VisualParameters();

        try {
            System.out.println("Parse with StAX");
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader =
                    factory.createXMLEventReader(new FileReader("src/main/resources/xml/Catalog.xml"));

            while(eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();

                switch(event.getEventType()) {

                    case XMLStreamConstants.START_ELEMENT:
                        StartElement startElement = event.asStartElement();
                        String qName = startElement.getName().getLocalPart();

                        if (qName.equalsIgnoreCase("gem")) {
                        } else if (qName.equalsIgnoreCase("name")) {
                            bName = true;
                        } else if (qName.equalsIgnoreCase("preciousness")) {
                            bPreciousness = true;
                        } else if (qName.equalsIgnoreCase("origin")) {
                            bOrigin = true;
                        }
                        else if (qName.equalsIgnoreCase("color")) {
                            bColor = true;
                        }
                        else if (qName.equalsIgnoreCase("transparency")) {
                            bTransparency = true;
                        }
                        else if (qName.equalsIgnoreCase("faces")) {
                            bFaces = true;
                        }
                        else if (qName.equalsIgnoreCase("value")) {
                            bValue = true;
                        }
                        break;

                    case XMLStreamConstants.CHARACTERS:
                        Characters characters = event.asCharacters();
                        if(bName) {
                            gem.setName(characters.getData());
                            bName = false;
                        }
                        if(bPreciousness) {
                            gem.setPreciousness(characters.getData().equals("precious")?
                                    Preciousness.precious:Preciousness.semiPrecious);
                            bPreciousness = false;
                        }
                        if(bOrigin) {
                            gem.setOrigin(characters.getData());
                            bOrigin = false;
                        }
                        if(bColor) {
                            vp.setColor(characters.getData());
                            bColor = false;
                        }
                        if(bTransparency) {
                            vp.setTransparency(new Double(characters.getData()));
                            bTransparency = false;
                        }
                        if(bFaces) {
                            vp.setNumberOfFaces(new Integer(characters.getData()));
                            gem.setVisualParameters(vp);
                            bFaces= false;
                        }
                        if(bValue) {
                            vp.setTransparency(new Double(characters.getData()));
                            bValue = false;
                        }
                        break;

                    case XMLStreamConstants.END_ELEMENT:
                        EndElement endElement = event.asEndElement();

                        if(endElement.getName().getLocalPart().equalsIgnoreCase("gem")) {
                            System.out.println(gem);
                        }
                        break;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        System.out.println("End parse with StAX");
    }
}
