package parsers;

import domain.Gem;
import domain.Preciousness;
import domain.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class ParserDOM {

    private DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    private DocumentBuilder builder = factory.newDocumentBuilder();

    public ParserDOM() throws ParserConfigurationException {
    }

    public void start() throws IOException, SAXException {
        Document document = builder.parse(new File("src/main/resources/xml/Catalog.xml"));

        document.getDocumentElement().normalize();

        Element root = document.getDocumentElement();
        System.out.println(root.getNodeName());

        NodeList nList = document.getElementsByTagName("gem");
        System.out.println("Parse with DOM parser");

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node node = nList.item(temp);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                Gem gem = new Gem();
                VisualParameters vp = new VisualParameters();

                gem.setName(element.getElementsByTagName("name").item(0).getTextContent());
                if (element.getElementsByTagName("preciousness").item(0).getTextContent().equals("precious")) {
                    gem.setPreciousness(Preciousness.precious);
                } else gem.setPreciousness(Preciousness.semiPrecious);
                gem.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());

                vp.setColor(element.getElementsByTagName("color").item(0).getTextContent());
                vp.setTransparency(new Double(element.getElementsByTagName("transparency").item(0).getTextContent()));
                vp.setNumberOfFaces(new Integer(element.getElementsByTagName("faces").item(0).getTextContent()));
                gem.setVisualParameters(vp);

                gem.setValue(new Integer(element.getElementsByTagName("value").item(0).getTextContent()));
                System.out.println(gem);
            }
        }
        System.out.println("It was DOM parser");

    }
}