import org.xml.sax.SAXException;
import parsers.ParserDOM;
import parsers.ParserSAX;
import parsers.ParserStAX;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
        new ParserSAX().start();
        new ParserDOM().start();
        new ParserStAX().start();
    }
}
